<?php

/**
 * @file
 *   Fingerprint page callbacks
 */

/**
 * Sends the node to a remote server.
 * The function uses the fingerprint_server, fingerprint_services_path Drupal variables to identify the server.
 * It uses the fingerprint_domain and fingerprint_key variables for authentication. If those are empty or not valid
 * a new key will be registered.
 *
 * Callback for node/%node/fingerprint_send
 */
function fingerprint_send(StdClass $node) {
  $url = variable_get('fingerprint_server', FINGERPRINT_DEFAULT_SERVER);
  $url .= variable_get('fingerprint_services_path', FINGERPRINT_DEFAULT_SERVICES_PATH);

  $domain = variable_get('fingerprint_domain', '');
  $key = variable_get('fingerprint_key', '');

  module_load_include('inc', 'fingerprint', 'fingerprint_xmlrpc');
  $xmlrpc_handler = new FingerprintXmlrpc($domain, $key, $url);
  if (!$xmlrpc_handler->isSessionActive()) {
    return t('Can\'t connect to server. Please !check_url.', array('!check_url' => l('check the server url', 'admin/settings/fingerprint'))) .'<br />'. t('Error message: %message', array('%message' => xmlrpc_error_msg()));
  }

  /** Check if the key is an anoymous key, register if so **/
  if (empty($domain) || empty($key) || $xmlrpc_handler->keyIsAnonymous()) {
    $key = $xmlrpc_handler->registerKey();
    if ($key) {
      variable_set('fingerprint_key', $key['kid']);
      variable_set('fingerprint_domain', $key['domain']);
      drupal_set_message('Registration successfull!');
    }
    else {
      return t('Registering key failed');
    }
  }

  // we'll need it later
  $nid = $node->nid;

  fingerprint_send_prepare($node);

  $node->fingerprint_key = $xmlrpc_handler->getKey();
  $remote_node = $xmlrpc_handler->send('fingerprint.save', $node);
  if (!$remote_node) {
    return t('Failed to save node: %message', array('%message' => xmlrpc_error_msg()));
  }

  // If it's a new node we save the remote_nid
  if ($remote_node['is_new']) {
    db_query('INSERT INTO {fingerprint}(nid, remote_nid) VALUES(%d, %d)', $nid, $remote_node['nid']);
  }

  $remote_node_path = variable_get('fingerprint_server', FINGERPRINT_DEFAULT_SERVER) .'?q=node/'. $remote_node['nid'];
  return t('Node saved as !url', array('!url' => l('node/'. $remote_node['nid'], $remote_node_path)));
}


/**
 * Prepares the node for sending by fingerprint.save
 */
function fingerprint_send_prepare(&$node) {
  // Not necessary to send and takes a lot of space
  unset($node->fingerprint);

  /**
   * If we heaven't saved the node yet, we'll create a new one
   * For that we have to remove the nid, else we woud just update
   */
  if ($node->fingerprint_remote_nid === FALSE) {
    unset($node->nid);
  }
  else {
    $node->nid = $node->fingerprint_remote_nid;
  }

  // Have to set changed, else the version on the other server will be never, and node can't be saved
  $node->changed = time();
}


/**
 * System settings form of the module
 *
 * Callback for admin/settings/fingerprint
 */
function fingerprint_settings_form($form_state) {
  $form = array();

  $form['fingerprint_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Server'),
    '#default_value' => variable_get('fingerprint_server', FINGERPRINT_DEFAULT_DOMAIN),
    '#description' => t('If you want to use your own fingerprint store, the following services permissions should be enabled: %permissions', array('%permissions' => 'system.connect, fingerprint.save')),
    '#required' => TRUE,
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['fingerprint_services_path'] = array(
    '#type' => 'textfield',
    '#title' => t('xmlrpc path'),
    '#default_value' => variable_get('fingerprint_services_path', FINGERPRINT_DEFAULT_SERVICES_PATH),
    '#description' => t('It can be clean url path, but not clean url is safer.'),
    '#required' => TRUE,
  );

  $form['advanced']['fingerprint_domain'] = array( // We're using the username as the domain as well
    '#type' => 'textfield',
    '#title' => t('Domain'),
    '#default_value' => variable_get('fingerprint_domain', ''),
    '#description' => t('Leave blank for asking a new from server'),
  );

  $form['advanced']['fingerprint_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Api key'),
    '#default_value' => variable_get('fingerprint_key', ''),
    '#description' => t('Leave blank for asking a new from server'),
  );

  $form['#submit'] = array('fingerprint_settings_form_submit');

  return system_settings_form($form);
}


/**
 * Checks if the settings changed. If the remote credentials are different,
 * the fingerprint table has to be flushed (remote nids won't match).
 */
function fingerprint_settings_form_submit($form, &$form_state) {
  // items in the form that should match the variables table
  // key => default value
  $matching_form_items = array(
    'fingerprint_server' => FINGERPRINT_DEFAULT_SERVER,
    'fingerprint_services_path' => FINGERPRINT_DEFAULT_SERVICES_PATH,
    'fingerprint_domain' => '',
    'fingerprint_key' => '',
  );

  $settings_updated = FALSE;
  foreach ($matching_form_items as $item => $default_value) {
    if ($form_state['values'][$item] != variable_get($item, $default_value)) {
      $settings_updated = TRUE;
    }
  }

  if ($settings_updated) {
    db_query('DELETE FROM {fingerprint}');
  }
}


/**
 * Lists nodes that the current node can be diffed for
 *
 * Callback for node/%/fingerprint_diff
 */
function fingerprint_diff_list(StdClass $node) {
  global $user;
  $other_nodes = array();

  $result = db_query(db_rewrite_sql('SELECT nid, title FROM {node} n WHERE type="fingerprint" AND nid<>%d AND uid=%d'), $node->nid, $user->uid);
  while ($other_node= db_fetch_object($result)) {
    $other_nodes[] = l($other_node->title, 'node/'. $node->nid .'/fingerprint_diff/'. $other_node->nid);
  }

  return theme('item_list', $other_nodes);
}


/**
 * Shows the difference between two fingerprint node
 *
 * Callback for node/%/fingerprint_diff/%
 *
 * @param $current
 *   int Current node id
 * @param $other
 *   int Other node id
 * @todo handle drupal_set_title in menu hook
 * @todo remove recursive references (since the hash will always be a difference)
 */
function fingerprint_view_diff($current, $other) {
  $current = node_load((int)$current);
  $other = node_load((int)$other);

  if (!(node_access('view', $current) || node_access('view', $other))) { // @todo handle it from access callback!!!
    drupal_access_denied();
  }

  drupal_set_title(t('Difference between "@current_title" and "@other_title"', array('@current_title' => $current->title, '@other_title' => $other->title)));

  // Emulate node render
  fingerprint_nodeapi($current, 'view', NULL, NULL, FINGERPRINT_NOHIGHLIGHT);
  fingerprint_nodeapi($other, 'view', NULL, NULL, FINGERPRINT_NOHIGHLIGHT);
  // Decoding '<' and '>' chars so it can be decoded
  $current->body = htmlspecialchars_decode($current->content['body']['#value']);
  $other->body = htmlspecialchars_decode($other->content['body']['#value']);

  module_load_include('inc', 'diff', 'diff.pages');
  $rows = _diff_body_rows($other, $current);
  $cols = _diff_default_cols();
  $header = _diff_default_header();

  return theme('diff_table', $header, $rows, array('class' => 'diff'), NULL, $cols);
}


/**
 * Callback for node/%node/fingerprint_download
 *
 * It serves the pure xml file for download
 */
function fingerprint_download($node) {
  /** Sending xml header **/
  ob_clean();
  header('Content-type: text/xml');
  header('Content-Disposition: attachment; filename="'. $node->title .'.xml"');

  /** Sending output **/
  module_load_include('inc', 'fingerprint', 'fingerprint_output_xml');
  $exporter = new FingerprintXml($node->fingerprint);
  echo htmlspecialchars_decode(FingerprintXml::formatXml($exporter->getOutput(), FINGERPRINT_NOHIGHLIGHT));
  ob_flush();

  /** Invoke exit hook **/
  module_invoke_all('exit');
  ob_clean(); // Clear the output buffer, so modules can't add garbage after the xml
  exit;
}


/**
 * Xpath filter form
 */
function fingerprint_xpath_filter_form($form_state) {
  $form = array();

  $form['xpath'] = array(
    '#type' => 'textfield',
    '#title' => t('Xpath filter'),
    '#default_value' => $_SESSION['fingerprint_xpath'],
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Filter'));

  return $form;
}


function fingerprint_xpath_filter_form_submit($form, &$form_state) {
  $_SESSION['fingerprint_xpath'] = $form_state['values']['xpath'];
}

