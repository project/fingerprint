<?php

/**
 * Key authentication is required by the services function
 * @see FingerprintXmlrpc::send()
 */
define('FINGERPRINT_KEY_REQUIRED', TRUE);

/**
 * Key authentication is not required by the services function
 * @see FingerprintXmlrpc::send()
 */
define('FINGERPRINT_KEY_NOT_REQUIRED', FALSE);

/**
 * @file
 *   XMLRPC handler class for the fingerprint module
 *   Originally from drupal.org/node/774298
 */
class FingerprintXmlrpc {

  /**
   * Services key id
   * @var String
   */
  private $kid;
  function setKey($key) {$this->kid = $key;}
  function getKey() {return $this->kid;}

  /**
   * Services domain
   * @var String
   */
  private $domain;
  function setDomain($domain) {$this->domain = $domain;}
  function getDomain() {return $this->domain;}


  /**
   * Constructs the xmlrpc handling object.
   * It automatically establishes the connection to the endpoint.
   *
   * @param String $domain
   *   Domain for services authentication
   * @param String $apiKey
   *   Services key id for authentication
   * @param String $endPoint
   *   Services xmlrpc url
   */
  function __construct($domain = '', $apiKey = '', $endPoint = '') {
    include_once 'includes/xmlrpc.inc';
    $this->domain = $domain;
    $this->kid = $apiKey;
    $this->endpoint = $endPoint;

    $this->log('domain: '. $domain .'; apiKey: '. $apiKey);

    $retVal = $this->send('system.connect', array(), FINGERPRINT_KEY_NOT_REQUIRED);
    $this->session_id = $retVal['sessid'];

    if ($this->session_id) {
      $this->log('Got anonymous session id fine');
    }
    else {
      $this->log('Failed to get anonymous session id!');
    }
  }


  /**
   * Logs debug messages
   * @param $log
   *   Debug message (any type), if null or no parameter it returns the logs
   */
  function log($log = NULL) {
    static $internal_log = array();
    if ($log === NULL) {
      return $internal_log;
    }

    $backtrace = debug_backtrace();
    $entry = array(
      'message' => $backtrace[1]['function'] .': '. $log,
    );

    if (xmlrpc_errno()) {
      $entry += array(
        'xmlrpc_errno' => xmlrpc_errno(),
        'xmlrpc_error_message' => xmlrpc_error_msg(),
      );
    }
    $internal_log[] = $entry;
  }



  /**
   * Checks if we could connect to the server or not
   *
   * @return bool
   *   TRUE if the connection active, FALSE otherwise
   */
  public function isSessionActive() {
    return isset($this->session_id);
  }


  /**
   * Checks it the key is an anonymous key
   *
   * @return bool
   *   TRUE if key is an anonymous key, FALSE otherwise
   */
  public function keyIsAnonymous() {
    return $this->send('fingerprint.isAnonymousKey', $this->kid, FINGERPRINT_KEY_NOT_REQUIRED);
  }


  /**
   * Function for sending xmlrpc requests
   *
   * @param methodName
   *   String xmlrpc method name
   * @param functionArgs
   *   Array Additional arguments in an array
   *   Alternatively any type that will be automatically convert to an array with one element
   * @return
   *   Mixed xmlrpc response
   */
  public function send($methodName, $functionArgs = array(), $keyRequired = FINGERPRINT_KEY_REQUIRED) {
    if (!is_array($functionArgs)) {
      $functionArgs = array($functionArgs);
    }

    $protocolArgs = array();

    // Check if the function does not require a key
    if ($keyRequired == FINGERPRINT_KEY_NOT_REQUIRED) {
      $protocolArgs = array($this->endpoint, $methodName);
    }
    else {
      $timestamp = (string)time();
      $nonce = $this->getUniqueCode();

      // prepare a hash
      $hash_parameters = array($timestamp, $this->domain, $nonce, $methodName);
      $hash = hash_hmac("sha256", implode(';', $hash_parameters), $this->kid);

      // prepared the arguments for this service:
      // note, the sessid needs to be the one returned by user.login
      $protocolArgs = array($this->endpoint, $methodName, $hash, $this->domain, $timestamp, $nonce, $this->session_id);
    }

    $params = array_merge($protocolArgs, $functionArgs);
    $ret = call_user_func_array('xmlrpc', $params);
    $this->log($methodName .'('. str_replace("\n", '', var_export($params, TRUE)) .") returned:\n". var_export($ret, TRUE));
    return $ret;
  }


  /**
   * login and return user object
   *
   * @param $userName
   *   String Remote user name
   * @param $userPass
   *   String Remote user password
   * @return
   *   Mixed, The user object if login succesfull, else FALSE
   */
  public function userLogin($userName = '', $userPass = '') {
    $this->log('DrupalXmlrpc->userLogin() called with userName "'. $userName .'" and pass "'. $userPass .'"');

    // clear out any lingering xmlrpc errors:
    xmlrpc_error( NULL, NULL, TRUE );

    $retVal = $this->send('user.login', array($userName, $userPass));
    if (!$retVal && xmlrpc_errno()) {
      return FALSE;
    }
    else {
      // remember our logged in session id:
      $this->session_id = $retVal['sessid'];

      // we might need the user object later, so save it:
      $user = new stdClass();
      $user = (object)$retVal['user'];
      $this->authenticated_user = $user;
      return $user;
    }
  }


  public function userLogout() {
    $retVal = $this->send( 'user.logout', array());
    if (!$retVal) {
      return FALSE;
    }

    return TRUE;
  }


  /**
   * Function for generating a random string, used for
   * generating a token for the XML-RPC session
   */
  private function getUniqueCode($length = 10) {
    return user_password($length);
  }


  /**
   * Registers a key at the remote server
   *
   * @param $userKeys
   *   To use the registered key in the object automatically
   * @return
   *   Key object
   *   @see fingerprint_services_register_key()
   */
  public function registerKey($useKeys = TRUE) {
    $key = $this->send('fingerprint.registerKey', FINGERPRINT_KEY_NOT_REQUIRED);
    if ($key && $useKeys) {
      $this->setKey($key['kid']);
      $this->setDomain($key['domain']);
    }

    return $key;
  }
}
